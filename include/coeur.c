#include <Arduino.h>
#include "param.h"

void setup_coeur(int pinLed[]){                               //Boucle d'initialisation des pins de sorties dans le tableau et mise à 0V des leds
  for (int i = 0; i < 10; i++)                                // i va nous servir pour parcourir le tableau, on parcour les cases une par une jusqu'à la derniere
    {
      pinMode(pinLed[i], OUTPUT);                             //on trabsforme une case en sortie
      digitalWrite(pinLed[i], LOW);                           // on lui donne un ordre, ici la mise à 0V
    }
  }

void coeur(int pinLed[], int bpm){
  if (MODE <= 5)                                              // Ici on à la sélection du mode de clignotement, les 5 premiers modes on le meme fonctionnement avec des sauts plus ou plus grands
    {
      int bps=50;                                    //selection bps qui represente la frequence cardiaque
      for (int i = 0; i < 10; i=i+MODE)                       // boucle for pour parcourir les cases du tableau, qui rappelons le representent chacune une led
    {
      digitalWrite(pinLed[i], HIGH);                          //on allume la LED correspondante
      delay(bps);
      digitalWrite(pinLed[i], LOW);                           //on éteint la correspondante
    }
  }
else{                                                         //suite de la séléction du mode, on utilise un switch car les differents modes sont differents
      int bps = 75;
      switch(MODE){
        case 6:
          for (int i = 0; i < 10; i++){
            digitalWrite(pinLed[i], HIGH);
          }
         delay(bps);
          for (int i = 0; i < 10; i++){
            digitalWrite(pinLed[i], LOW);
          }
         delay(bps);
        break;
        
        case 7:
          for (int i = 0; i < 5; i++){
            digitalWrite(pinLed[i], HIGH);
          }
         delay(bps);
          for (int i = 0; i < 5; i++){
            digitalWrite(pinLed[i], LOW);
          }
          for (int i = 5; i < 10; i++){
            digitalWrite(pinLed[i], HIGH);
          }
        delay(bps);
          for (int i = 4; i < 10; i++){
            digitalWrite(pinLed[i], LOW);
          }   
         break;

        default:
        break; 
    }
  }
}
